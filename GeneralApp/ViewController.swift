//
//  ViewController.swift
//  GeneralApp
//
//  Created by MacBook Pro on 3/18/20.
//  Copyright © 2020 Test. All rights reserved.
//

import UIKit
import LanguageManager_iOS
class ViewController: UIViewController{
    
    
    @IBOutlet weak var arabicButton: UIButton!
    @IBOutlet weak var englishButton: UIButton!
    @IBOutlet weak var titleLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        /// Uncomment the following line to see Localizable.strings behaviour
        //  userLocalizableStringsInsteadOffStoryboard()
    }
    
    func userLocalizableStringsInsteadOffStoryboard() {
        arabicButton.setTitle("arabic".localiz(), for: .normal)
        englishButton.setTitle("english".localiz(), for: .normal)
        titleLabel.text = "title".localiz()
        
    }
    
    @IBAction func onArabicClicked() {
        changeLanguage(.ar)
    }
    
    @IBAction func onEnglishClicked() {
        changeLanguage(.en)
    }
    
    func changeLanguage(_ selectedLanguage: Languages) {
        LanguageManager.shared.setLanguage(language: selectedLanguage, viewControllerFactory: {_ in
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            return  storyboard.instantiateInitialViewController()!
        })
    }

}

