//
//  AppDelegate.swift
//  GeneralApp
//
//  Created by MacBook Pro on 3/18/20.
//  Copyright © 2020 Test. All rights reserved.
//

import UIKit
import LanguageManager_iOS
@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        LanguageManager.shared.defaultLanguage = .deviceLanguage

        return true
    }




}

